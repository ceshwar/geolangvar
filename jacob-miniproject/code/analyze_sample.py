
#load the samples
file_a = ""
file_b = ""

#list of unique words
vocabulary = []

#dict with frequency counts for each sample
vocab_sample_a = {}
vocab_sample_b = {}

#get the vocabulary for Sample A
with open(file_a, 'r') as tweetlist: 
	#read Tweets from the file one by one

	#Use Twokenize
	print "Size of Tweet List = ", len(tweetlist) 
	for tweets in tweetlist:
        	tweettokens = tokenizeRawTweetText(tweets)
        	print "No. of tokens in the Tweet = ", len(tweettokens)
       	 	#print the tokens in the tweet
        	print "The tokens in the tweet are:"
        	
		for token in tweettokens:
			print token
			#update vocabulary
			if not token in vocabulary:
				vocabulary.append(token)

			if token in vocab_sample_a:
			#update frequency value 
				vocab_sample_a[token] = vocab_sample_a[token] + 1
			else:
				#first occurrence of the token
				vocab_sample_a[token] = 1

	        print "End of Tweet"

print "Unique Words in Vocabulary: " , len(vocabulary)
print "Vocabulary Size of sample: ", len(vocab_sample_a)

with open(file_b, 'r') as tweetlist:
        #read Tweets from the file one by one

        #Use Twokenize
        print "Size of Tweet List = ", len(tweetlist)
        for tweets in tweetlist:
                tweettokens = tokenizeRawTweetText(tweets)
                print "No. of tokens in the Tweet = ", len(tweettokens)
                #print the tokens in the tweet
                print "The tokens in the tweet are:"

                for token in tweettokens:
                        print token
                        #update vocabulary
                        if not token in vocabulary:
                                vocabulary.append(token)

                        if token in vocab_sample_b:
                        #update frequency value 
                                vocab_sample_b[token] = vocab_sample_b[token] + 1
                        else:
                                #first occurrence of the token
                                vocab_sample_b[token] = 1

                print "End of Tweet"

print "Unique Words in Vocabulary: " , len(vocabulary)
print "Vocabulary Size of Sample: ", len(vocab_sample_b)


#Compute the distance metrics between the samples: vocab_sample_a VS vocab_sample_b

#1) L1 - distance:

l1_dist = 0

for word in vocabulary:
	if(word in vocab_sample_a):
		f1 = vocab_sample_a[word]
	else:
		f1 = 0
	
	if(word in vocab_sample_b):
		f2 = vocab_sample_b[word]
	else:
		f2 = 0

	if f1 > f2:
		l1_dist = l1_dist + f1 - f2	
	else:
		l1_dist = l1_dist + f2 - f1

print "L1-distance between Sample A and Sample B = ", l1_dist
