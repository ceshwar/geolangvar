import json
from twokenize import tokenizeRawTweetText
#Using the Twokenize code from https://github.com/myleott/ark-twokenize-py/blob/master/twokenize.py
 
with open('sample-tweets.json', 'r') as f:
    count = 0
    hits = 0
    print "IT BEGINS!"
    tweetlist = []

    for line in f:
	#line = f.readline() # read only the first tweet/line
	tweet = json.loads(line) # load it as Python dict
    	#print(json.dumps(tweet, indent=4)) # pretty-print

	#count the total no. of tweets seen so far
	count = count + 1
	#hashtags = []
	#get tweet and required metadata from the tweet json object
	try:
		text = tweet["text"]
		lang = tweet["lang"]
		userlocation = tweet["user"]["location"]
		time_created = tweet["created_at"]
		#get the hashtags
		numtags = len(tweet["entities"]["hashtags"])
		if(numtags < 1):
			hashtags = None
		else: 
			hashtags = []
			for i in range(0,numtags):
				hashtags.append(tweet["entities"]["hashtags"][i]["text"])
	except KeyError, e:
		text = "Null"
		lang = "Null"
		userlocation = "Null"
		time_created = "Null"

	#get the hashtags		
	#for i in range(0,numtags-1):
		#hashtags = (tweet['entities']['hashtags'][i]['text']

	#control for the location provided by the user and also the language of the tweet (en)
	if(userlocation == "New York" or userlocation == "Los Angeles" or userlocation == "Atlanta"):
		if(lang == 'en'):
			hits = hits + 1
			print "Language of the Tweet: " + lang
   			print "The Tweet itself: " + text
			print "User Location: " + userlocation
			print "Time at which Tweet came: " + time_created
			if(numtags > 0):
				print "Hashtags used: "
				for hashtag in hashtags:
					print hashtag
			tweetlist.append(text)
#	print_string = "{0:12d}, {1:2s}".format(actor_id,language_code)
#	print(print_string)
    print "Total No. of Tweets = " + str(count)
    print "No. of Tweets which are Hits! = " + str(hits)

    #Use Twokenize
    print "Size of Tweet List = ", len(tweetlist) 
    for tweets in tweetlist:
	tweettokens = tokenizeRawTweetText(tweets)
	print "No. of tokens in the Tweet = ", len(tweettokens)
	#print the tokens in the tweet
	print "The tokens in the tweet are:"
	for token in tweettokens:
		print token

	print "End of Tweet"
	
	# Summary:
	# Now have the code to read .JSON file of tweets; 
    	# Filter tweets by Language of the tweet and the Location field provided by the User
	# Get the Hashtags present in the Tweet
	# Twokenize the Text content in each Tweet

	# To Do:
	# Compute "Frequency scores" for each token across the entire volume of tweets
	# Identify the MOST occurring tokens or Keywords in these tweets
	# See if you can compare two sets of Tweets based on the frequency scores obtained for each set - Chi-Squared Test or Mann-Whitney Rank Test.
