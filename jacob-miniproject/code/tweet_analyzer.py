import json
import gzip

#location of tweet.gz
#f = "tweets/Feb14/tweets-Feb-10-14-00-00.gz"
#f = "tweets/Feb15/tweets-Feb-10-15-00-00.gz"
hits = 0
la = 0
ny = 0
tweet_count = 0
urls = 0
images = 0
notenglish = 0
retweet = 0
spam_account = 0
follow_limit = 1000

flag = 0

location_miss = 0
line_miss = 0
lang_miss =0
follower_miss =0
retweet_miss = 0
url_miss = 0

tweet_limit = 500000
print "Loading files"

#prefix = "tweets/Feb14/"
prefix = "tweets/Feb15/"
#inputfile_list = "sample_14.txt"
inputfile_list = "sample_15.txt"
inputs = open(inputfile_list)
debug = 1

for f in iter(inputs):
 f = f.strip('\n')
 f = prefix + f
 with gzip.open(f,'rb') as fin:
                print "Loaded file!"
                print f

                for line in fin:
                        #if(tweet_count > limit):
                        #       break
                        #print line
                        try:
                                tweet = json.loads(line)
                                #jdict = decoder.decode(line.rstrip()) #this will give the JSON dict
                        except:
                                print "Line Miss!"
                                line_miss = line_miss + 1
                                continue

                        tweet_count = tweet_count + 1
                        #print "Tweet# ", tweet_count
                        flag = 0

                        try:
                                userlocation = tweet["user"]["location"]
                                userlocation = userlocation.lower()
                        except KeyError, e:
                                userlocation = "Null"
                                location_miss = location_miss + 1

                                #get the hashtags
                                #numtags = len(tweet["entities"]["hashtags"])
                                #if(numtags < 1):
                                #       hashtags = None
                                #else:
                                #       hashtags = []
                                #       for i in range(0,numtags):
                                 #              hashtags.append(tweet["entities"]["hashtags"][i]["text"])

        #control for the location provided by the user and also the language of the tweet (en)
                        #if userlocation != "Null": #example only

                        if("york" in userlocation or "new york" in userlocation or "ny" in userlocation or "newyork" in userlocation):
                                hits = hits + 1
                                ny = ny + 1
                                flag =  1
                        if( "los angeles" in userlocation or "la" in userlocation or "losangeles" in userlocation):
                                hits = hits + 1
                                la = la + 1
                             flag = 1

                        if(flag != 1):
                                continue

                        try:
                                #text = tweet["text"]
                                lang = tweet["lang"]
                                if(lang != "en"):
                                        notenglish = notenglish + 1
                        except KeyError, e:
                                lang_miss = lang_miss + 1

                        try:
                               if(tweet["user"]["followers_count"] >= follow_limit or tweet["user"]["friends_count"] >= follow_limit):
                                      spam_account = spam_account + 1
                                      #continue
                        except KeyError, e:
                               follower_miss  = follower_miss + 1

                        try:
                                if(len(tweet["entities"]["url"]) != 0):
                                        urls = urls + 1
                        except KeyError, e:
                                url_miss = url_miss + 1

                        try:
                                if(tweet["retweeted"]):
                                        retweet = retweet + 1
                                 #continue
                        except KeyError, e:
                                retweet_miss = retweet_miss + 1

                #............

print "Overall tweets = ", tweet_count
print "Total Hits = " , hits
print "Hits in NY = ", ny
print "Hits in LA = ", la

print "-------------------------"
print "Filtering stats - Posts that were not considered"
print "Non-English posts = " , notenglish
print "Spam account posts = " , spam_account
print "URL posts = " , urls
print "Retweeted posts = " , retweet
print "-------------------------"

print "Key Errors observed"
print "Line miss = " , line_miss
print "Location miss = " , location_miss
print "Language miss = " , lang_miss
print "Follower miss = " , follower_miss
print "URL miss = " , url_miss
print "Retweet miss = " , retweet_miss
print "-------------------------"
                                                                                                                                                                                    142,0-1       B
