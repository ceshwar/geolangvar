import json
import gzip

def print_stats():
 print "---------BEGIN----------------"
 print "Days: " + str(start_date) + " TO " + str(end_date)+ " of Month: " + month + " . Year: 20"+str(year)
 print "Overall tweets considered= ", tweet_count
 print "Total Hits = " , hits
 print "Hits in NY = ", ny
 print "Hits in LA = ", la
 
 print "Total unique users in LA = ", total_unique_users_la
 print "Total unique users in NY = ", total_unique_users_ny
 
 print "---------STATS----------------"
 print "Total no. of tweets read= ", total_tweets_read
 print "Filtering stats - Posts that were not considered"
 print "Non-English posts = " , notenglish
 print "Spam account posts = " , spam_account
 print "URL posts = " , urls
 print "Retweeted posts = " , retweet
 print "-------------------------"

 print "Key Errors observed"
 print "Line miss = " , line_miss
 print "Text miss = " , text_miss
 print "Location miss = " , location_miss
 print "Language miss = " , lang_miss
 print "Follower miss = " , follower_miss
 print "URL miss = " , url_miss
 print "Retweet miss = " , retweet_miss
 print "Write Miss = ", write_miss
 print "---------THE END----------------"
 return 1


output_log = "outputlog.txt"
write_output = open(output_log, "w")

def print_to_file():
 
 write_output.write("---------BEGIN----------------\n")
 write_output.write("Days: " + str(start_date) + " TO " + str(end_date)+ " of Month: " + month + " . Year: 20"+str(year) + "\n")
 write_output.write("Overall tweets considered= " + str(tweet_count) + "\n")
 write_output.write("Total Hits = " + str( hits) + "\n")
 write_output.write("Hits in NY = "+str(ny) + "\n")
 write_output.write("Hits in LA = "+ str(la) + "\n")

 write_output.write("Total unique users in LA = "+str(total_unique_users_la) + "\n")
 write_output.write("Total unique users in NY = " + str(total_unique_users_ny) + "\n")
 #write_output.write("New users this month in LA = "  + str(new_users_that_month_la) + "\n")
 #write_output.write("New users this month in NY = " + str(new_users_that_month_ny) + "\n")

 write_output.write("---------STATS----------------")
 write_output.write("Total no. of tweets read= " + str(total_tweets_read) + "\n")
 write_output.write("Filtering stats - Posts that were not considered\n")
 write_output.write("Non-English posts = " + str(notenglish) + "\n")
 write_output.write("Spam account posts = " + str(spam_account) + "\n")
 write_output.write("URL posts = " + str(urls) + "\n")
 write_output.write("Retweeted posts = " + str(retweet) + "\n")
 write_output.write("-------------------------\n")

 write_output.write("Key Errors observed\n")
 write_output.write("Line miss = " + str(line_miss) + "\n")
 write_output.write("Text miss = "+ str(text_miss) + "\n")
 write_output.write("Location miss = " + str(location_miss) + "\n")
 write_output.write("Language miss = " + str(lang_miss)+"\n")
 write_output.write("Follower miss = " + str(follower_miss)+"\n")
 write_output.write("URL miss = " +  str(url_miss) + "\n")
 write_output.write("Retweet miss = " + str(retweet_miss) + "\n")
 write_output.write("Write Miss = " + str(write_miss) + "\n")
 write_output.write("---------THE END----------------\n")
 return 1

#location of tweet.gz
#f = "tweets/Feb14/tweets-Feb-10-14-00-00.gz"

file_location = "/nethome/corpora/twitter-crawl/archive-from-arizona/"
year = 14
month = "Mar"
start_date = 1
end_date = 10

month_list = ["Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov","Dec","Jan","Feb","Mar"]
end_date_list = [31,30,31,30,31,31,30,31,30,31,31,28,31]
month_num = 0

#months to iterate over
start_month = 0
end_month = 12
#end_month = 12

month_file_dict = {}

#november modified file names
month_file_dict["824"] = "-04-14.gz"
month_file_dict["825"] = "-04-42.gz"
month_file_dict["826"] = "-03-47.gz"
month_file_dict["827"] = "-04-34.gz"
month_file_dict["828"] = "-03-58.gz"
month_file_dict["829"] = "-03-20.gz"
month_file_dict["830"] = "-03-50.gz"

##
#december modified file names
month_file_dict["91"] = "-03-42.gz"
month_file_dict["92"] = "-04-07.gz"
month_file_dict["93"] = "-03-26.gz"
month_file_dict["94"] = "-03-51.gz"
month_file_dict["95"] = "-03-32.gz"
month_file_dict["96"] = "-04-02.gz"
month_file_dict["97"] = "-03-24.gz"
month_file_dict["98"] = "-03-27.gz"
month_file_dict["99"] = "-04-15.gz"
month_file_dict["910"] = "-04-04.gz"
month_file_dict["911"] = "-04-06.gz"
month_file_dict["912"] = "-03-56.gz"
month_file_dict["913"] = "-03-52.gz"
month_file_dict["914"] = "-04-28.gz"
month_file_dict["915"] = "-04-07.gz"
month_file_dict["916"] = "-04-20.gz"
month_file_dict["917"] = "-03-28.gz"

#f = "tweets-Feb-10-15-00-00.gz"

#####
file_starting = "tests_begin.txt"
write_starting = open(file_starting, "w")
write_starting.write("LET THE GAMES BEGIN!\n")
write_starting.close()
####

#for year in range(14,16):
for month_num in range(start_month,end_month+1):
 month = month_list[month_num]
 end_date = end_date_list[month_num]
 year = 14
 if(month_num > 9):
	year = 15
 #print "Month num = " + str(month_num) + ", Month = " + month + ", Year = " + str(year )
 #update these variables for each month!
 #new_users_that_month_la = 0
 #new_users_that_month_ny = 0
 ###

 user_id_list_la = []
 total_unique_users_la = 0
 user_id_list_ny = []
 total_unique_users_ny = 0
 ####

 hits = 0
 la = 0
 ny = 0
 total_tweets_read = 0
 tweet_count = 0
 urls = 0
 images = 0
 notenglish = 0
 retweet = 0
 spam_account = 0
 follow_limit = 10000

 flag = 0
 
 line_miss = 0
 text_miss = 0
 location_miss = 0
 lang_miss =0
 follower_miss =0
 retweet_miss = 0
 url_miss = 0
 write_miss = 0

 
 sample_la = "monthly_tweets/tweets_"+month+"_20" + str(year)+ "_la.txt"
 sample_ny = "monthly_tweets/tweets_"+month+"_20" + str(year) + "_ny.txt"
 users_la = "monthly_users/users_"+month+"_20" + str(year)+ "_la.txt"
 users_ny = "monthly_users/users_"+month+"_20" + str(year)+ "_ny.txt"

 write_la = open(sample_la, "w")
 write_ny = open(sample_ny, "w")
 write_user_la = open(users_la, "w")
 write_user_ny = open(users_ny, "w")

 #tweet_limit = 0
 #tweet_limit = 5000
 print "Load files:"
 write_output.write("load files:\n")
 
 for date in range(start_date,end_date+1):
  #tweet_limit = tweet_limit + 5000
 
  if(date < 10):
 	fill_date = "0"+str(date)
  else:
	fill_date = str(date)
  try: 
	#print month_file_dict[str(month_num)+str(date)]
	f = file_location+"tweets-"+month+"-"+fill_date+"-"+str(year)+month_file_dict[str(month_num)+str(date)]
  except:
	f = file_location+"tweets-"+month+"-"+fill_date+"-"+str(year)+"-00-00.gz"

  #try:
  with gzip.open(f,'rb') as fin:		
		print "Loading file: " + f
		write_output.write("Loading file: " + f +"\n")
		check_open = 0
  		#stop = 1
  		#if(stop == 1):
		#	continue
		for line in fin:
			#print line
               	   	try:
                	        tweet = json.loads(line)
				#jdict = decoder.decode(line.rstrip()) #this will give the JSON dict
     		  	except:
				#print "Line Miss!"
				line_miss = line_miss + 1
				continue
			if(check_open == 0):
				print "ITS OPEN!"
				check_open = 1

        		total_tweets_read = total_tweets_read + 1
                        #print "Tweet# ", total_tweets_read
                        flag = 0

			try:
				text = tweet["text"]
 			except KeyError, e: 
				#print "Text Miss" + text
                                text = "Null"
				text_miss = text_miss + 1
                                continue

			try:
                		userlocation = tweet["user"]["location"]
                		userlocation = userlocation.lower()
			except KeyError, e:
				userlocation = "Null"
				location_miss = location_miss + 1
				continue
			try:
				user_id = tweet["user"]["id"]
			except KeyError,e:
				user_id = -1

			## BEGIN DATA PREPROCESSING - FILTERING
                        try:
                               if(tweet["user"]["followers_count"] >= follow_limit or tweet["user"]["friends_count"] >= follow_limit):
                                      spam_account = spam_account + 1
                                      flag = -1
				      continue
                        except KeyError, e:
                               follower_miss  = follower_miss + 1

                        try:
                                if(len(tweet["entities"]["url"]) != 0):
                                        urls = urls + 1
                                        flag = -1
					continue
                        except KeyError, e:
                                url_miss = url_miss + 1
			
			try:
                	        if(tweet["retweeted"]):
                                        retweet = retweet + 1
                                        flag = -1
					continue
                        except KeyError, e:
                                retweet_miss = retweet_miss + 1
			###############
			import re
			#retweets
			if re.search(r'\bRT\b', text):
		        	retweet = retweet + 1
   				flag = -1
				continue
			#URLs
			if re.search(r'\bhttps?:', text, re.I):
        			urls = urls + 1
				flag = -1
				continue
         	        try:
                                #text = tweet["text"]
                                lang = tweet["lang"]
                                if(lang != "en"):
                                        notenglish = notenglish + 1
                                        flag = -1
					continue
                        except KeyError, e:
                                lang_miss = lang_miss + 1
	
			#END OF FILTERING!
		  	if(flag == -1):
				continue
			
			 #location filtering
			#total no. of tweets considered after preprocessing
			tweet_count = tweet_count + 1

                        if(userlocation == "nyc" or userlocation == "ny city" or "new york" in userlocation  or "newyork" in userlocation or userlocation == "ny"):
                                hits = hits + 1
                                ny = ny + 1
                                flag =  1
                                #print "NY in the form of : " + userlocation
                        if( "los angeles" in userlocation or "losangeles" in userlocation or userlocation == "la"):
                                hits = hits + 1
                                la = la + 1
                                flag = 2
                                #print "LA in the form of : " + userlocation
			if(flag == 1):
				#NY
				try:
					write_ny.write(text.lower()+"\n")		
				except:
					#print "Write Miss"
					ny = ny - 1
					hits = hits - 1
					write_miss = write_miss + 1
					continue
					#print text
					#write_ny.write(text.encode('utf-8').lower()+"\n")		
			if(flag == 2):
				#LA
				try:
					write_la.write(text.lower()+"\n")
				except:
					#print "Write Miss"
					la = la - 1
					hits = hits - 1
					write_miss = write_miss + 1
					continue
					#print text
					#write_la.write(text.encode('utf-8').lower()+"\n")		
			
			#update list of users:
                        if(flag == 1 and user_id != -1 and user_id not in user_id_list_ny):
                               	user_id_list_ny.append(user_id)
                               	total_unique_users_ny = total_unique_users_ny + 1
                       		write_user_ny.write(str(user_id) + "\n") 
			if(flag == 2 and user_id != -1 and user_id not in user_id_list_la):
                                user_id_list_la.append(user_id)
                                total_unique_users_la = total_unique_users_la + 1
				write_user_la.write(str(user_id) + "\n")
		#............
  #except:
  #	print "No such file bro! " + f 
###
  write_ny.close()
  write_la.close()
  write_user_ny.close()
  write_user_la.close()
###
 print_stats()
 print_to_file()

write_output.close()

file_ending = "tests_ended.txt"
write_ending = open(file_ending, "w")
write_ending.write("THAT'S ALL FOLKS!\n")
write_ending.close()
