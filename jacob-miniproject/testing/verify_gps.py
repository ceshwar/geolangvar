import json
import gzip

#http://stackoverflow.com/questions/15736995/how-can-i-quickly-estimate-the-distance-between-two-latitude-longitude-points
from math import radians, cos, sin, asin, sqrt
def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    km = 6367 * c
    return km

def print_stats():
	print "Location Hits = " + str(location_hits)
	print "Location Miss = " + str(location_miss)
	print "GPS Hits = " + str(gps_hits)
	print "GPS Miss = " + str(gps_miss)
	print "Within Radius (50 km) = " + str(within_radius)
	print "Percentage Match between Location field and GPS: " , 100.0 * float(within_radius)/gps_hits
	print "Total tweets read = " + str(total_tweets_read)
	print "Line Miss = " + str(line_miss)
	return 1

file_location = "tweets-Mar-01-14-00-00.gz"
hits = 0
fails = 0
location_hits = 0
location_miss = 0
line_miss = 0
gps_hits = 0
gps_miss = 0

total_tweets_read = 0

limit = 1000
window = 10

#GPS coordinates for NYC
ny_lon = -74.005941
ny_lat = 40.7127837

within_radius = 0

with gzip.open(file_location, 'rb') as fin:
	print "Loaded file: " + file_location
	for line in fin:
		try:
			tweet = json.loads(line)
		except:
			line_miss = line_miss + 1
			continue
		
		if(gps_hits > limit):
			break
		
		total_tweets_read = total_tweets_read + 1
		
		try:
			userlocation = tweet['user']['location'].lower()
		except:
			location_miss = location_miss + 1
			continue
		
		if(userlocation == "nyc" or userlocation == "ny city" or "new york" in userlocation or "newyork" in userlocation or userlocation == "ny"):
			location_hits = location_hits + 1
		else:
			continue	
		try:	
			coords = tweet['coordinates']
		except:
			gps_miss = gps_miss + 1
			continue
		###
		#print temp stats
		window = window-1
		if(window < 0):
			print_stats()
			window = 10
		###
		if coords is not None:
			#print str(coords['coordinates'][0]) + "," + str(coords['coordinates'][1])
			#print tweet['coordinates']
			gps_hits = gps_hits + 1
			#get distance from New York
			lon = coords['coordinates'][0]
			lat = coords['coordinates'][1]
			haversine_dist = haversine(ny_lon, ny_lat, lon, lat)
			#print "Distance from NY in km  = ", haversine_dist
			if(haversine_dist < 50):
				within_radius = within_radius + 1
			#else:
				#print "!! " + userlocation
		else:
			gps_miss = gps_miss + 1


