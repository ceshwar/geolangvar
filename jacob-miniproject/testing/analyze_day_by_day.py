from twokenize import tokenizeRawTweetText
#Using the Twokenize code from https://github.com/myleott/ark-twokenize-py/blob/master/twokenize.py

#load the samples
start_date = 1
end_date = 10

month = "Mar"
#year = 14
year = 15
#file_a = "filtered_tweets_"+month+"_"+fill_date+"_20" + str(year)+ "_ny.txt"
#file_b = "filtered_tweets_"+month+"_"+fill_date+"_20" + str(year)+ "_la.txt"

#file_a = "temp_tweets_"+year+"_ny.txt"
#file_b = "temp_tweets_"+year+"_la.txt"
avg_l1_dist = 0.0

for date in range(start_date, end_date+1):
 #list of unique words
 vocabulary = []

 #dict with frequency counts for each sample
 vocab_sample_a = {}
 vocab_sample_b = {}

 num_tweets_a = 0
 num_tweets_b = 0

 total_frequency_a = 0
 total_frequency_b = 0

 #analyze for tweets collected over days
 if(date < 10):
	fill_date = "0"+str(date)
 else:
	fill_date = str(date)

 file_a = "tweets/filtered_tweets_"+month+"_"+fill_date+"_20" + str(year)+ "_ny.txt"
 file_b = "tweets/filtered_tweets_"+month+"_"+fill_date+"_20" + str(year)+ "_la.txt"
 
 #get the vocabulary for Sample A
 with open(file_a, 'r') as tweetlist: 
	#read Tweets from the file one by one
	#print "Load Sample A"
	#Use Twokenize
	for tweets in tweetlist:
		tweettokens = tokenizeRawTweetText(tweets)
		num_tweets_a = num_tweets_a + 1
        	#print "No. of tokens in the Tweet = ", len(tweettokens)
       	 	#print the tokens in the tweet
        	#print "The tokens in the tweet are:"
 	
		for token in tweettokens:
			#print token
			#filtering
			#update vocabulary

			if not token in vocabulary:
				vocabulary.append(token)

			if token in vocab_sample_a:
			#update frequency value 
				vocab_sample_a[token] = vocab_sample_a[token] + 1
			else:
				#first occurrence of the token
				vocab_sample_a[token] = 1

	        #print "End of Tweet"

 with open(file_b, 'r') as tweetlist:
        #read Tweets from the file one by one
	#print "Load Sample B"
        #Use Twokenize
        for tweets in tweetlist:
		tweettokens = tokenizeRawTweetText(tweets)
		num_tweets_b = num_tweets_b + 1
                #print "No. of tokens in the Tweet = ", len(tweettokens)
                #print the tokens in the tweet
                #print "The tokens in the tweet are:"
		for token in tweettokens:
                        #print token
                        #update vocabulary
                        if not token in vocabulary:
                                vocabulary.append(token)

                        if token in vocab_sample_b:
                        #update frequency value 
                                vocab_sample_b[token] = vocab_sample_b[token] + 1
                        else:
                                #first occurrence of the token
                                vocab_sample_b[token] = 1

                #print "End of Tweet"

############
#print most frequent words from each sample
 from collections import OrderedDict
 sorted_vocab_sample_a = OrderedDict(sorted(vocab_sample_a.items(), key=lambda x: (-x[1], x[0])))
 sorted_vocab_sample_b = OrderedDict(sorted(vocab_sample_b.items(), key=lambda x: (-x[1], x[0])))

 top_num = 0
 #print "Top " + str(top_num) + " words in Sample A"
 tmp_cnt = 0
 for word in sorted_vocab_sample_a:
 	if(tmp_cnt >= top_num):
		break

	tmp_cnt = tmp_cnt + 1
	print "Word = " + word
	print " Frequency = ", sorted_vocab_sample_a[word]
	print ""

 #print "Top " + str(top_num) + " words in Sample B"
 tmp_cnt = 0
 for word in sorted_vocab_sample_b:
        if(tmp_cnt >= top_num):
                break

        tmp_cnt = tmp_cnt + 1
        print "Word = " + word
	print " Frequency = ",sorted_vocab_sample_b[word]
	print ""
 ######
 #Compute total frequencies for each sample to get the frequency rates instead of just using the counts!
 for word in vocab_sample_a:
        total_frequency_a = total_frequency_a + vocab_sample_a[word]
 for word in vocab_sample_b:
        total_frequency_b = total_frequency_b + vocab_sample_b[word]
 #########

 #Compute the distance metrics between the samples: vocab_sample_a VS vocab_sample_b

 #1) L1 - distance:
 l1_dist = 0.0

 for word in vocabulary:
        if(word in vocab_sample_a):
                f1 = vocab_sample_a[word]/float(total_frequency_a)
        else:
                f1 = 0.0

        if(word in vocab_sample_b):
                f2 = vocab_sample_b[word]/float(total_frequency_b)
        else:
                f2 = 0.0

        if f1 > f2:
                l1_dist = l1_dist + f1 - f2
        else:
                l1_dist = l1_dist + f2 - f1

 print "Tweets from " + fill_date + " " +  month + " , 20" + str(year)

 print "No. of tweets from Sample A: " , num_tweets_a
 print "No. of tweets from Sample B: " ,  num_tweets_b
 print "Total No. of tweets from both samples: " ,  (num_tweets_b + num_tweets_a)
 print "Unique Words in Vocabulary: " , len(vocabulary)
 print "Vocabulary Size of Sample A: ", len(vocab_sample_a)
 print "Vocabulary Size of sample B: ", len(vocab_sample_b)

 print "L1-distance between Sample A and Sample B = ", l1_dist
 avg_l1_dist = avg_l1_dist + l1_dist

avg_l1_dist = avg_l1_dist / float(end_date + 1 - start_date)
print "Average L1-Distance over all days = ", avg_l1_dist
